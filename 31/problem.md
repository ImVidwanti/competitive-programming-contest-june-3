### Attack of the Aliens

Aliens are attacking Earth! And you are in charge of the planetary defense team. You man a gun that shoots at the alien space ships as they approach Earth.

The problem is that the gun is fixed in position. But you can change the angle of your sight. And another advantage is that the gun is so powerful that if the bullet fired from the gun hits any part of a spaceship, the ship is destroyed instantly.

Also, the bullets from the gun travel in an exact straight line along the angle of sight, and are not affected by little things like gravity or wind resistance. __(Unrealistic, I know but bear with me. :) )__

**Input Format**

The first line of input consists of the integer **__T__** which is the number of test cases.  Each test case begins with the line **__N W H__**. Here **__W__** is the width of the arena, **__H__** is the height of the arena and **__N__** is the number of spaceships attacking Earth and within the arena. Note that if the spaceship flies out of the arena i.e. no part of the ship is within the arena, then it can’t be hit.

Then N lines follow, with each line containing 4 integers **__Li Vi Xi Yi__**.

Here **__Li__** is the length of the ith spaceship measured in metres, **__Vi__** is its velocity in metres per second, and **__(Xi, Yi)__** are the coordinates of the position of the center of the ship. __(Take the origin to be the position of the gun, and all lengths measured in metres.)__

The velocity can be positive or negative. A negative velocity implies that the spaceship travels to the left, and a positive velocity implies that the spaceship travels to the right.

The spaceship can only move in a horizontal direction.

After these lines, the next line of input consists of two integers **__M G__**. Here **__M__** is the number of shots fired from the gun at one second time intervals and **__G__** is the length of the gun. A bullet fired will come out from the end of the barrel!

Then M lines follow, consisting of two integers **__Ai and Si__**. **__Ai__** represents the angle the gun barrel makes with respect to the Y axis when the ith bullet is fired from it, and **__Si__** is the speed at which the ith bullet comes out from the barrel.

**__Ai is measured in 1/60th parts of a degree (minutes of arc.)__**

All in all, each test case looks like this:

N W H

L0 V0 X0 Y0

L1 V1 X1 Y1

.

.

.

LN-1 VN-1 XN-1 YN-1

M G

A0 S0

A1 S1

.

.

.

AM-1 SM-1

![](images/problem31.png)

**Output Format**

For each test case, output an integer D which is the number of ships that are destroyed.


**Constraints**

1 <= T <= 100

1 <= N <= 1000

100 <= H <= 1000

100 <= W <= 1000

1 <= Li <= 100

1 <= M <= 20

5 <= G <=20

(-W + Li)/2<= Xi <= (W-Li)/2

10 + G <= Yi <= H

-50 <= Vi <= 50

-3600 <= Ai <= 3600

10 <= Si <= 200

**Assumptions**

* The bullets are not affected by wind or gravity; and neither are the ships

* The position of the bullet t seconds after it’s fired is given by (x(t), y(t)) where these co-ordinates are correct to two decimal places. So if the position of any part of a ship coincides (correct to two decimal places) with the position of a bullet at time t, the ship is considered to be blown up (including the end of the spaceship of course!)

* Note that all the Yis are different. That is, no two spaceships are the same height above ground

* Note also that all the Ais are different. No two bullets can be fired from the same angle

* If a bullet destroys a ship, it cannot destroy the ships above it. One shot, only one kill, in other words!

* Assume each ship is perfectly flat, and that the gun barrel has no thickness (Just length!)

* A negative velocity of a spaceship implies that the ship is moving towards the left, while a positive velocity implies that the ship is moving towards the right

* The position of the base of the gun is at (0,0)


**Sample Input**

**Sample Output**
