In the country of Venland, the currency unit is vens. The currency comes in coins of four distinct denominations: x0, x1, x2, and x3 vens. Find out the number of different ways to represent X vens given that the total number of coins you have is Y. (That is, the number of coins of denominations x0, x1, x2, x3 totals Y; you can use fewer than Y, but you can’t exceed Y.)

**Input Format**

The first line of input consists of an integer T which is the number of test cases. Then T lines follow with each test case consisting of 6 integers X Y x0 x1 x2 x3

**Output Format**

For each test case, output a single integer M which is the number of different ways to represent X vens.

**Constraints**

1 <= X <= 10^9

4 <= Y <= 10^9

1 <= xi <= 10^9

**Sample Input**
```
3
5 5 1 2 3 4
5 4 1 2 3 4
11 4 1 2 3 4
```
**Sample Output**
```
3
2
0
```